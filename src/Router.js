import React from 'react';
import { StyleSheet, Platform, TouchableOpacity, Text } from 'react-native';
import { StackNavigator, NavigationActions } from 'react-navigation';
import Homepage from './components/Homepage';
import LoginForm from './components/LoginForm';
import AssetsList from './components/assets/AssetsList';
import AssetShow from './components/assets/AssetShow';
import AlarmSets from './components/AlarmSets';
import DgaCalculator from './components/DgaCalculator';
import SamplesList from './components/samples/SamplesList';
import ProfilePage from './components/ProfilePage';
import SamplesView from './components/samples/SamplesView';
import AddSample from './components/samples/AddSample';
// import Test from './components/Test';
//Get the ForgotPassword
import ForgotPassword from './components/ForgotPassword';
// Get the Diagnostics
import Diagnostics from './components/Diagnostics';
// import AddSample from './components/AddSample';

//Get the ZoneList
import ZoneList from './components/ZoneList';

import CriticalAssets from './components/CriticalAssets';

//Get the EditSample
import EditSample from './components/samples/EditSample';

//Get AddSubstation
import AddSubstation from './components/AddSubstation';

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'login' })
  ]
});
const RouterComponent = StackNavigator({
  login: {
    screen: LoginForm,
    navigationOptions: {
      header: null
    }
  },
  forgotPassword: {
    screen: ForgotPassword,
    navigationOptions: {
      header: null
    }
  },
  homePage: {
    screen: Homepage,
    navigationOptions: ({ navigation }) => ({
      title: 'Trafix Home',
      headerLeft: null,
      headerRight: <TouchableOpacity onPress={() => navigation.dispatch(resetAction)}>
        <Text style={{ color: '#fff', fontWeight: 'bold', marginRight: 10 }}>Logout</Text>
      </TouchableOpacity>
    })
  },
  addSubstation: {
    screen: AddSubstation,
    navigationOptions: () => ({
      title: 'Add Substation'
    })
  },
  criticalAssets: {
    screen: CriticalAssets,
    navigationOptions: () => ({
      title: 'Critical Assets'
    })
  },
  assetsList: {
    screen: AssetsList,
    navigationOptions: () => ({
      title: 'Your Assets'
    })
  },
  zoneList: {
    screen: ZoneList,
    navigationOptions: () => ({
      title: 'Zone List'
    })
  },
  assetShow: {
    screen: AssetShow,
    navigationOptions: () => ({
      title: 'Asset View'
    })
  },
  samplesList: {
    screen: SamplesList,
    navigationOptions: ({ navigation }) => ({
      title: 'Sample List',
      headerRight: <TouchableOpacity onPress={() => navigation.navigate('addSample')}>
        <Text style={{ color: '#fff', fontWeight: 'bold', marginRight: 10 }}>Add Sample</Text>
      </TouchableOpacity>
    })
  },
  addSample: {
    screen: AddSample,
    navigationOptions: () => ({
      title: 'Add Sample',
    })
  },
  alarmSets: {
    screen: AlarmSets,
    navigationOptions: () => ({
      title: 'AlarmSets'
    })
  },
  dgaCalculator: {
    screen: DgaCalculator,
    navigationOptions: () => ({
      title: 'Dga Calculator'
    })
  },
  samplesView: {
    screen: SamplesView,
    navigationOptions: () => ({
      title: 'Samples Values'
    })
  },
  editSample: {
    screen: EditSample,
    navigationOptions: () => ({
      title: 'Edit Samples'
    })
  },
  diagnostics: {
    screen: Diagnostics,
    navigationOptions: () => ({
      title: 'Diagnostics Report'
    })
  },
  profilePage: {
    screen: ProfilePage,
    navigationOptions: () => ({
      title: 'Profile'
    })
  }
},
{ navigationOptions: () => ({
    headerTintColor: 'white',
    headerStyle: styles.header
  })
}
);

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#414551'
  },
  headerSearch: {
    ...Platform.select({
      android: {
        paddingTop: 52
      },
      ios: {
        paddingTop: 52
      }
    })
  },
  headerPush: {
    ...Platform.select({
      android: {
        paddingTop: 50
      },
      ios: {
        paddingTop: 62
      }
    })
  }
});

export default RouterComponent;
