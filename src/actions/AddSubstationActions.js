/*global fetch:false*/
import { Actions } from 'react-native-router-flux';
import {
  SUBSTATION_CHANGED,
  SUBSTATION_POST_SUCCESS,
  LOAD_SUB_SPINNER,
  SUBSTATION_POST_FAILED
} from './types';
import { API_ENDPOINT } from './api';
import NavigatorService from '../services/navigator';

export const substationChanged = (text) => {
  return {
    type: SUBSTATION_CHANGED,
    payload: text
  };
};

export const substationPost = ({ latitude, longitude, substation }) => {
  return (dispatch, getState) => {
    dispatch({
      type: LOAD_SUB_SPINNER
    });
    fetch(`${API_ENDPOINT}addsubstation`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      },
      body: JSON.stringify({
        substation: {
          longitude,
          latitude,
          substation
        }
      })
    })
    .then(() => {
      dispatch({
        type: SUBSTATION_POST_SUCCESS
      });
      // Actions.homepage();
      NavigatorService.navigate('homePage');
    })
    .catch(() => {
      dispatch({
        type: SUBSTATION_POST_FAILED
      });
    });
  };
};
