/*global fetch:false*/
import { Actions } from 'react-native-router-flux';
import {
  ASSETS_FETCHED_SUCCESS,
  ASSET_FETCHED_SUCCESS,
  LOAD_ASSET_SPINNER,
  ASSET_ID_SELECT
} from './types';
import { API_ENDPOINT, MAIN_LINK } from './api';
import NavigatorService from '../services/navigator';

export const assetsFetch = () => {
  return (dispatch, getState) => {
    dispatch({
      type: LOAD_ASSET_SPINNER
    });
    fetch(`${API_ENDPOINT}assets`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      }
    }).then(response => {
      response.json().then(data => {
        console.log(data);
        dispatch({
          type: ASSETS_FETCHED_SUCCESS,
          payload: data
        });
      });
    });
  };
};

export const selectAsset = (assetId) => {
  console.log('ASSET ID:', assetId);
  return (dispatch) => {
    dispatch({
      type: ASSET_ID_SELECT,
      payload: assetId
    });
    // Actions.assetShow();
    NavigatorService.navigate('assetShow');
  };
};

// Select asset
// Get the asset.
export const assetFetch = () => {
  return (dispatch, getState) => {
    const selectedAsset = getState().assets.selectedAsset;
    dispatch({
      type: LOAD_ASSET_SPINNER
    });
    fetch(`${API_ENDPOINT}assets/${selectedAsset}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      }
    }).then(response => {
      response.json().then(data => {
        console.log('Single Asset DATA:', data);
        dispatch({
          type: ASSET_FETCHED_SUCCESS,
          payload: {
            singleAsset: data
          }
        });
      });
    });
  };
};

// I need to work on this function. Probably remove it entirely.
export const getReport = () => {
  return (dispatch, getState) => {
    fetch(`${MAIN_LINK}en/assets/${getState().assets.selectedAsset}.pdf`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      }
    }).then(response => {
      response.json().then(data => {
        console.log(data);
        // dispatch({
        //   type: ASSETS_FETCHED_SUCCESS,
        //   payload: data
        // });
      });
    });
  };
};
