/*global fetch:false*/
import _ from 'lodash';
import { Actions } from 'react-native-router-flux';
import NavigatorService from '../services/navigator';

import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_FAILED,
  LOGIN_USER_SUCCESS,
  LOAD_SPINNER
} from './types';
import { API_TOKEN } from './api';

export const emailChanged = (text) => {
  const a = text.charAt(0).toLowerCase() + text.slice(1);
  return {
    type: EMAIL_CHANGED,
    payload: a
  };
};

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

export const loginUser = ({ email, password }) => {
  const mainemail = _.lowerFirst(email).trim();
  return (dispatch) => {
    dispatch({
      type: LOAD_SPINNER
    });
    fetch(API_TOKEN, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          user: {
            username: mainemail,
            password,
            // email: 'sankalpsingha',
            // password: 'sankalp123!@#'
          }
        })
      })
      .then(user => {
        if (user.status === 401) {
          dispatch({
            type: LOGIN_USER_FAILED
          });
        } else {
          // Send the user inside
          user.json().then((data) => {
            dispatch({
              type: LOGIN_USER_SUCCESS,
              payload: data
            });
            // Actions.main({ type: 'reset' });
              console.log('Success');
            NavigatorService.reset('homePage');
          });
        }
      }).catch((error) => {
        console.log(error);
        dispatch({
          type: LOGIN_USER_FAILED
        });
      });
  };
};

export const forgotEmail = (values) => {
  return (dispatch) => {
    dispatch({
      type: 'forgot_password',
      payload: values
    });
    // Actions.login();
    NavigatorService.navigate('login');
  };
};
