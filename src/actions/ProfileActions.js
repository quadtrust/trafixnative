/*global fetch:false*/
import { API_ENDPOINT } from './api';
import { PROFILE_FETCHED_SUCCESS } from './types';

export const profileFetch = () => {
  return (dispatch, getState) => {
    // Fetch the profile...
    fetch(`${API_ENDPOINT}/user`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      }
    }).then(response => {
      response.json().then(data => {
        console.log(data);
        dispatch({
          type: PROFILE_FETCHED_SUCCESS,
          payload: data
        });
      });
    });
  };
};
