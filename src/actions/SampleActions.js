/*global fetch:false*/
import { Actions } from 'react-native-router-flux';
import { API_ENDPOINT } from './api';
import {
  SAMPLES_FETCHED_SUCCESS,
  SAMPLE_ID_SELECT,
  SAMPLE_FETCHED_SUCCESS,
  DIAGNOSTICS_FETCHED_SUCCESS,
  SAMPLE_UPDATE,
  SAMPLE_CREATE,
  SAVE_EDIT_SAMPLE
} from './types';
import NavigatorService from '../services/navigator';

export const samplesFetch = () => {
  return (dispatch, getState) => {
    console.log('Selected asset from the samples:', getState().assets.selectedAsset);
    fetch(`${API_ENDPOINT}assets/${getState().assets.selectedAsset}/samples`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      }
    }).then(response => {
      response.json().then(data => {
        console.log(data);
        dispatch({
          type: SAMPLES_FETCHED_SUCCESS,
          payload: data
        });
      });
    });
  };
};

export const selectSample = (sampleId) => {
  console.log('SAMPLE ID:', sampleId);
  return (dispatch) => {
    dispatch({
      type: SAMPLE_ID_SELECT,
      payload: sampleId
    });
    // Actions.samplesView();
    NavigatorService.navigate('samplesView');
  };
};

export const fetchSample = () => {
  return (dispatch, getState) => {
    console.log('Selected sample from the samples:', getState().samples.selectedSample);
    const selectedID = getState().samples.selectedSample;
    fetch(`${API_ENDPOINT}assets/${getState().assets.selectedAsset}/samples/${selectedID}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      }
    }).then(response => {
      response.json().then(data => {
        console.log(data);
        dispatch({
          type: SAMPLE_FETCHED_SUCCESS,
          payload: {
            singleSample: data
          }
        });
      });
    });
  };
};


export const getDiagnostics = () => {
  return (dispatch, getState) => {
    const selectedID = getState().samples.selectedSample;
    const selectedAsset = getState().assets.selectedAsset;
    console.log('The link: ', `${API_ENDPOINT}assets/${selectedAsset}/samples/${selectedID}/diagnostics`);
    fetch(`${API_ENDPOINT}assets/${selectedAsset}/samples/${selectedID}/diagnostics`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      }
    }).then(response => {
      response.json().then(data => {
        console.log(data);
        dispatch({
          type: DIAGNOSTICS_FETCHED_SUCCESS,
          payload: data
        });
      });
    });
  };
};

export const sampleUpdate = ({ prop, value }) => {
  return {
    type: SAMPLE_UPDATE,
    payload: { prop, value }
  };
};

export const createSample = ({ source_type,
  sample_date,
  sample_number,
  n2,
  o2,
  ch4,
  c2h2,
  c2h4,
  co2,
  co,
  c2h6,
  h2,
  moisture
 }) => {
  return (dispatch, getState) => {
    const selectedAsset = getState().assets.selectedAsset;
    fetch(`${API_ENDPOINT}assets/${selectedAsset}/samples`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      },
      body: JSON.stringify({
        sample: {
          sample_date,
          sample_number,
          source_type,
          n2,
          o2,
          ch4,
          c2h2,
          c2h4,
          co2,
          co,
          c2h6,
          h2,
          moisture
        }
      })
    })
    .then(() => {
      dispatch({
        type: SAMPLE_CREATE
      });
      // Actions.samplesList({ type: 'reset' });
      NavigatorService.navigate('samplesList');
    });
  };
};

export const saveSample = ({ sample_date,
  sample_number,
  source_type,
  n2,
  o2,
  ch4,
  c2h2,
  c2h4,
  co2,
  co,
  c2h6,
  h2,
  moisture }) => {
  return (dispatch, getState) => {
    const selectedAsset = getState().assets.selectedAsset;
    const selectedID = getState().samples.selectedSample;

    fetch(`${API_ENDPOINT}assets/${selectedAsset}/samples/${selectedID}`, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      },
      body: JSON.stringify({
        sample: {
          sample_date,
          sample_number,
          source_type,
          n2,
          o2,
          ch4,
          c2h2,
          c2h4,
          co2,
          co,
          c2h6,
          h2,
          moisture
        }
      })
    })
    .then(() => {
      dispatch({
        type: SAVE_EDIT_SAMPLE
      });
      console.log('Save!!');
      // Actions.samplesView();
      NavigatorService.navigate('samplesView');
    });
  };
};
