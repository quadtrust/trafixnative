/*global fetch:false*/
import {
  API_ENDPOINT
} from './api';

import {
  ZONES_FETCHED_SUCCESS,
} from './types';


export const fetchZones = () => {
  return (dispatch, getState) => {
    fetch(`${API_ENDPOINT}zones`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-User-Token': getState().auth.authentication_token,
        'X-User-Email': getState().auth.username
      }
    }).then(response => {
      response.json().then(data => {
        console.log('Single Asset DATA:', data);
        dispatch({
          type: ZONES_FETCHED_SUCCESS,
          payload: data
        });
      });
    });
  };
};
