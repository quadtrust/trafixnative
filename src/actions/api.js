export const MAIN_LINK = 'https://trafixsoft.com/';
// export const MAIN_LINK = 'http://localhost:3000/';

export const API_ENDPOINT = `${MAIN_LINK}api/v1/`;
export const API_TOKEN = `${MAIN_LINK}/token`;
