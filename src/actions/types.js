export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_FAILED = 'login_user_failed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOAD_SPINNER = 'load_spinner';

// For the assets page
export const LOAD_ASSET_SPINNER = 'load_asset_spinner';
export const ASSETS_FETCHED_SUCCESS = 'assets_fetched_success';

export const ASSET_ID_SELECT = 'ASSET_ID_SELECT';
export const ASSET_FETCHED_SUCCESS = 'asset_fetched_success';

// For the samples page
export const SAMPLES_FETCHED_SUCCESS = 'samples_fetched_success';
export const SAMPLE_ID_SELECT = 'sample_id_select';
export const SAMPLE_FETCHED_SUCCESS = 'sample_fetched_success';
export const SAVE_EDIT_SAMPLE = 'save_edit_sample';

export const PROFILE_FETCHED_SUCCESS = 'profile_fetched_success';

export const DIAGNOSTICS_FETCHED_SUCCESS = 'diagnostics_fetched_success';
export const SAMPLE_UPDATE = 'sample_update';
export const SAMPLE_CREATE = 'sample_create';

/// For the zones path.

export const ZONES_FETCHED_SUCCESS = 'zones_fetched_success';

//For the AddSubstation page
export const SUBSTATION_CHANGED = 'substation_changed';
export const SUBSTATION_POST_SUCCESS = 'substation_post_success';
export const SUBSTATION_POST_FAILED = 'substation_post_failed';
export const LOAD_SUB_SPINNER = 'load_substation_spinner';
