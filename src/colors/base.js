export const BACKGROUND_COLOR = '#282c34';
export const YELLOW = '#414551';
export const HEADER_COLOR = YELLOW;
export const BUTTON_GREY = '#32353e';
export const RGBA_RED = 'rgba(201, 48, 44, .5)';
export const RGBA_GREEN = 'rgba(68, 157, 68, 0.5)';
export const RGBA_YELLOW = 'rgba(236, 151, 31, 0.5)';
