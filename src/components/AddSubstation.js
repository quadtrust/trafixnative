/*global navigator:false*/

import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import _ from 'lodash';
import { Hoshi } from 'react-native-textinput-effects';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { substationChanged, substationPost } from '../actions';
import { BACKGROUND_COLOR } from '../colors/base';
import Spinner from './common/Spinner';

const WIDTH = Dimensions.get('window').width;

class AddSubstation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0
    };
  }
  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log(position);
        console.log('Latitude: ', position.coords.latitude);
        console.log('Logitude: ', position.coords.longitude);

        this.setState({
          longitude: position.coords.longitude,
          latitude: position.coords.latitude
        });
      },
      (error) => console.log(JSON.stringify(error))
    );
  }

  onButtonPress() {
    const { substation } = this.props;
    const { logitude, latitude } = this.state;
    this.props.substationPost({ logitude, latitude, substation });
  }

  onButtonPressUpdate() {
    this.setState({
      longitude: 0,
      latitude: 0
    });
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          longitude: position.coords.longitude,
          latitude: position.coords.latitude
        });
      },
      (error) => console.log(JSON.stringify(error))
    );
  }
  renderButton() {
    if (this.props.loading) {
      return (
        <View style={{ marginTop: 20, marginBottom: 20 }}>
          <Spinner />
        </View>
      );
    }
    return (
      <View style={{ marginTop: 50 }}>
        <TouchableOpacity style={[styles.button, { borderColor: 'green' }]} onPress={this.onButtonPress.bind(this)}>
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
  renderError() {
    if (this.props.error !== '') {
      return (
        <Text style={{ color: 'red', fontSize: 15 }}>{this.props.error}</Text>
      );
    }
      return null;
  }
  render() {
    return (
      <ScrollView style={styles.viewStyle}>
        <View style={{ alignItems: 'center' }}>
          <Icon name='sitemap'size={70} color="#fff" />
        </View>
        <Hoshi
          label={'Enter Substation'}
          // this is used as active and passive border color
          borderColor={'#fff'}
          labelStyle={{ color: '#fff', fontSize: 12 }}
          inputStyle={{ color: '#fff' }}
          value={this.props.substation}
          onChangeText={(text) => this.props.substationChanged(text)}
        />
        <Hoshi
          label={'Enter Longitude'}
          // this is used as active and passive border color
          keyboardEntry={'numeric'}
          borderColor={'#fff'}
          labelStyle={{ color: '#fff', fontSize: 12 }}
          inputStyle={{ color: '#fff' }}
          value={_.toString(this.state.longitude)}
        />
        <Hoshi
          label={'Enter Latitude'}
          // this is used as active and passive border color
          borderColor={'#fff'}
          labelStyle={{ color: '#fff', fontSize: 12 }}
          inputStyle={{ color: '#fff' }}
          value={_.toString(this.state.latitude)}
        />
        {this.renderError()}
        {this.renderButton()}

        <View style={{ marginBottom: 100, marginTop: 15 }}>
          <TouchableOpacity style={[styles.button, { borderColor: 'blue' }]} onPress={this.onButtonPressUpdate.bind(this)}>
            <Text style={styles.buttonText}>Update Substation</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    paddingTop: 65,
    paddingLeft: 20,
    paddingRight: 20
  },
  button: {
    borderRadius: 3,
    width: WIDTH - 40,
    borderWidth: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff',
    fontSize: 15
  }
});

function mapStateToProps(state) {
  const { substation, loading, error } = state.substations;
  // const { latitude, longitude } = state.substations.latlongitude;
  // console.log(latitude);
  return {
      substation,
      loading,
      error
  };
}
export default connect(mapStateToProps, { substationChanged, substationPost })(AddSubstation);
