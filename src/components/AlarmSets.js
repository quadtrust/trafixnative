import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { BACKGROUND_COLOR } from '../colors/base';

class AlarmSets extends Component {
  render() {
    return (
      <View style={styles.viewStyles}>
        <Icon name='bell' size={45} color="#fff" style={styles.bellStyle} />
        <View style={{ backgroundColor: '#4e5a7d', marginTop: 30 }}>
          <Text style={{ textAlign: 'center', fontSize: 20, color: '#fff', padding: 10 }}>
            DOBLE WARNING LEVELS
          </Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>H2</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>100</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>CH4</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>100</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>C2H6</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>60</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>C2H4</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>100</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>C2H2</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>5</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>CO</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>100</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>CO2</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>NA</Text>
          </View>

        </View>

        <View style={{ backgroundColor: '#4e5a7d', marginTop: 20 }}>
          <Text style={{ textAlign: 'center', fontSize: 20, color: '#fff', padding: 10 }}>
            IEC WARNING LEVELS
          </Text>
        </View>

        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>H2</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>150</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>CH4</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>110</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>C2H6</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>90</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>C2H4</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>280</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>C2H2</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>50</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>CO</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>900</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>CO2</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>13000</Text>
          </View>

        </View>


        <View style={{ backgroundColor: '#4e5a7d', marginTop: 20 }}>
          <Text style={{ textAlign: 'center', fontSize: 20, color: '#fff', padding: 10 }}>
            IEEE WARNING LEVELS
          </Text>
        </View>

        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>H2</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>100</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>CH4</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>120</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>C2H6</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>65</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>C2H4</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>50</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>C2H2</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>35</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>CO</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>350</Text>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#cc3333', fontWeight: 'bold' }}>CO2</Text>
            <Text style={{ color: '#fff', fontWeight: 'bold', marginTop: 15 }}>2500</Text>
          </View>

        </View>

      </View>
    );
  }
}

const styles = {
  viewStyles: {
    backgroundColor: BACKGROUND_COLOR,
    flex: 1
  },
  bellStyle: {
    alignSelf: 'center',
    paddingTop: 20
  }
};
export default AlarmSets;
