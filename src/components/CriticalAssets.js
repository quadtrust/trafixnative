/*global fetch:false*/
import _ from 'lodash';
import React, { Component } from 'react';
import { ListView, View, TouchableOpacity, TextInput, Text } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import { BACKGROUND_COLOR, BUTTON_GREY } from '../colors/base';

import Spinner from './common/Spinner';
import HealthImage from './misc/HealthImage';
import { assetsFetch, selectAsset } from '../actions';

const ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2
});

class AssetsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: ''
    };
  }

  componentDidMount() {
    this.props.assetsFetch();

    // Implementing the datasource for the list View
    // this.createDataSource(this.props.assets);
  }

  onSearchChange(text) {
    this.setState({
      searchTerm: text
    });
  }

  renderRow(asset) {
    if (asset.critical) {
    return (
      <TouchableOpacity onPress={() => { this.props.selectAsset(asset.id); }}>
        <View style={styles.assetListStyle}>
          <View style={{ flexDirection: 'row' }}>
            <HealthImage health={asset.health} />
            <View style={{ justifyContent: 'space-between', marginLeft: 20 }}>
              <Text style={{ color: '#CCC' }}>{asset.name}</Text>
              <Text style={{ color: '#CCC' }}>Serial No: {asset.serial_number} </Text>
              <Text style={{ fontSize: 10, color: '#CCC' }}>
                ({asset.total_samples} samples)
              </Text>
            </View>
          </View>
          <Icon
            name='ios-arrow-forward'
            size={45}
            color="#eee"
            // style={{
            //   alignSelf: 'flex-end'
            // }}
          />
        </View>
      </TouchableOpacity>
    );
  }
  return null;
  }

  renderSpinner() {
    if (this.props.spinner) {
      return (
        <View style={{ paddingTop: 20 }}>
          <Spinner />
        </View>
      );
    }
  }
  renderHeader() {
    return (
      <View style={styles.searchHeaderContainer}>
          <View style={styles.searchBar}>
              <Icon name="ios-search" size={30} />
              <TextInput
                placeholder="Search"
                underlineColorAndroid={'transparent'}
                onChangeText={this.onSearchChange.bind(this)}
                style={{ flex: 1, }}
              />
              <Icon name="ios-people" size={30} />
          </View>
          {/* <Button transparent>
              <Text style={{ color: '#f8f8f8' }}>Search</Text>
          </Button> */}
      </View>
    );
  }

  render() {
    const filteredAssets = this.state.searchTerm
      ? this.props.assets.filter(asset => {
          return asset.name.indexOf(this.state.searchTerm) > -1;
        })
      : this.props.assets;
    const dataSource = ds.cloneWithRows(filteredAssets);
    return (
      <View style={styles.viewStyle}>

        {this.renderSpinner()}

        <ListView
          enableEmptySections
          dataSource={dataSource}
          renderRow={this.renderRow.bind(this)} //https://github.com/facebook/react-native/issues/7233#issuecomment-214764329
          renderHeader={this.renderHeader.bind(this)}
        />

      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    assets: _.values(state.assets.asset),
    spinner: state.assets.asset_spinner
  };
};

const styles = {
  assetListStyle: {
    backgroundColor: BUTTON_GREY,
    marginBottom: 10,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  viewStyle: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
  },
  nText: {
    fontSize: 10,
    color: '#999999'
  },
  searchHeaderContainer: {
    backgroundColor: '#282c34',
    elevation: 5,
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  searchBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderRadius: 3,
    alignItems: 'center',
    paddingHorizontal: 5
  },
};

export default connect(mapStateToProps, { assetsFetch, selectAsset })(AssetsList);
