import React, { Component } from 'react';
import { ScrollView, View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import { Hoshi } from 'react-native-textinput-effects';
import DgaResult from './DgaResult';
import { BACKGROUND_COLOR } from '../colors/base';

const WIDTH = Dimensions.get('window').width;

class DgaCalculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      h2Val: '',
      ch4Val: '',
      c2h4Val: '',
      c2h2Val: '',
      c2h6Val: '',
      co2Val: '',
      n2Val: '',
      o2Val: '',
      coVal: '',
      tdcgText: '',
      rogerDiagnoText: '',
      rogerRatio1: '',
      rogerRatio2: '',
      rogerRatio3: '',
      doernenDiagnoText: '',
      doernenRatio1: '',
      doernenRatio2: '',
      doernenRatio3: '',
      doernenRatio4: '',
      basicGasDiagnoText: '',
      gasqoetient1: '',
      gasquotient2: '',
      gasquotient3: '',
      co2DiagnoText: '',
      co2Ratio: '',
      duvalDiagnoText: '',
      duval1: '',
      duval2: '',
      duval3: ''
    };
    this.onButtonPress = this.onButtonPress.bind(this);
  }

onPressClear() {
  this.setState({
    h2Val: '',
    ch4Val: '',
    c2h4Val: '',
    c2h2Val: '',
    c2h6Val: '',
    co2Val: '',
    n2Val: '',
    o2Val: '',
    coVal: '',
    tdcgText: '',
    rogerDiagnoText: '',
    rogerRatio1: '',
    rogerRatio2: '',
    rogerRatio3: '',
    doernenDiagnoText: '',
    doernenRatio1: '',
    doernenRatio2: '',
    doernenRatio3: '',
    doernenRatio4: '',
    basicGasDiagnoText: '',
    gasqoetient1: '',
    gasquotient2: '',
    gasquotient3: '',
    co2DiagnoText: '',
    co2Ratio: '',
    duvalDiagnoText: '',
    duval1: '',
    duval2: '',
    duval3: ''
  });
}
  onButtonPress() {
    console.log('Button is pressed!');
    // const diagnosis = '';
    const theta = '0.0001';
    const H2 = this.state.h2Val;
    const CH4 = this.state.ch4Val;
    const C2H6 = this.state.c2h6Val;
    const C2H2 = this.state.c2h2Val;
    const C2H4 = this.state.c2h4Val;
    const CO = this.state.coVal;
    const CO2 = this.state.co2Val;
    console.log(CO2);

    const R2 = Math.round((C2H2 / C2H4) * 100) / 100;
    const R1 = Math.round((CH4 / H2) * 100) / 100;
    const R5 = Math.round((C2H4 / C2H6) * 100) / 100;
    const R3 = Math.round((C2H2 / CH4) * 100) / 100;
    const R4 = Math.round((C2H6 / C2H2) * 100) / 100;

    const co2co = Math.round((CO2 / CO) * 100) / 100;

    const C2H2Percentage = Math.round((C2H2 / (Number(C2H2) + Number(C2H4) + Number(CH4))) * 100);
    const C2H4Percentage = Math.round((C2H4 / (Number(C2H2) + Number(C2H4) + Number(CH4))) * 100);
    const CH4Percentage = Math.round((CH4 / (Number(C2H2) + Number(C2H4) + Number(CH4))) * 100);
    let TDCG = Number(H2) + Number(CH4) + Number(C2H6) + Number(C2H4) + Number(C2H2) + Number(CO);
    TDCG = Math.round(TDCG * 100) / 100;
    console.log(TDCG);

    this.setState({ tdcgText: `TDCG (total dissolved combustible gases) = ${TDCG}` });

// Rogers
    if (R2 < 0.1 && R1 > 0.1 && R1 < 1.0 && R5 < 1.0) {
        this.setState({ rogerDiagnoText: 'Unit normal' });
    } else if (R2 < 0.1 && R1 < 0.1 && R5 < 1.0) {
        this.setState({ rogerDiagnoText: 'Low-energy density arcing--PD<sup>a</sup>' });
    } else if (R2 >= 0.1 && R2 <= 3.0 && R1 >= 0.1 && R1 <= 1.0 && R5 > 3.0) {
        this.setState({ rogerDiagnoText: 'Arcing--High-energy discharge' });
    } else if (R2 < 0.1 && R1 > 0.1 && R1 < 1.0 && R5 >= 1.0 && R5 <= 3.0) {
        this.setState({ rogerDiagnoText: 'Low temperature thermal' });
    } else if (R2 < 0.1 && R1 > 1.0 && R5 >= 1.0 && R5 <= 3.0) {
      this.setState({ rogerDiagnoText: 'Thermal<700Â°C' });
      } else if (R2 < 0.1 && R1 > 1.0 && R5 > 3.0) {
        this.setState({ rogerDiagnoText: 'Thermal>700Â°C' });
      } else {
        this.setState({ rogerDiagnoText: 'No clear diagnosis possible' });
    }

    if ((C2H4 > theta) && (H2 > theta) && (C2H6 > theta)) {
        this.setState({ rogerRatio1: `C2H2 / C2H4 = ${R2}` });
        this.setState({ rogerRatio2: `CH4 / H2 = ${R1}` });
        this.setState({ rogerRatio3: `C2H4 / C2H6 = ${R5}` });
    } else {
        this.setState({ rogerRatio1: 'C2H2 / C2H4 = -' });
        this.setState({ rogerRatio2: 'CH4 / H2 = -' });
        this.setState({ rogerRatio3: 'C2H4 / C2H6 = -' });
        this.setState({ rogerDiagnoText: 'No diagnosis possible' });
    }

//Doernenburg

    if (R1 > 1.0 && R2 < 0.75 && R3 < 0.3 && R4 > 0.4) {
        this.setState({ doernenDiagnoText: 'Thermal decomposition' });
    } else if (R1 < 0.1 && R3 < 0.3 && R4 > 0.4) {
        this.setState({ doernenDiagnoText: 'Partial discharge (low-intensity PD)' });
    } else if (R1 > 0.1 && R1 < 1.0 && R2 > 0.75 && R3 > 0.3 && R4 < 0.4) {
        this.setState({ doernenDiagnoText: 'Arcing (high-intensity PD)' });
    } else {
      this.setState({ doernenDiagnoText: 'No clear diagnosis possible' });
    }

    if ((C2H4 > theta) && (H2 > theta) && (CH4 > theta) && (C2H2 > theta)) {
        this.setState({ doernenRatio1: `C2H2 / C2H4 = ${R2}` });
        this.setState({ doernenRatio2: `CH4 / H2 = ${R1}` });
        this.setState({ doernenRatio3: `C2H2 / CH4 = ${R3}` });
        this.setState({ doernenRatio4: `C2H6 / C2H2 = ${R4}` });
    } else {
        this.setState({ doernenRatio1: 'C2H2 / C2H4 = -' });
        this.setState({ doernenRatio2: 'CH4 / H2 = -' });
        this.setState({ doernenRatio3: 'C2H2 / CH4 = -' });
        this.setState({ doernenRatio4: 'C2H6 / C2H2 = -' });
        this.setState({ doernenDiagnoText: 'No diagnosis possible' });
    }

//Basis-Gasquotienten
    if (R1 <= 0.1 && R5 <= 0.2) {
        this.setState({ basicGasDiagnoText: 'PD: Partial discharges' });
    } else if (R2 >= 1 && R1 >= 0.1 && R1 <= 0.5 && R5 >= 1) {
        this.setState({ basicGasDiagnoText: 'D1: Discharges of low energy' });
    } else if (R2 >= 0.6 && R2 < 2.5 && R1 >= 0.1 && R1 < 1 && R5 >= 2) {
        this.setState({ basicGasDiagnoText: 'D2: Discharges of high energy' });
    } else if (R5 < 1) {
        this.setState({ basicGasDiagnoText: 'T1: Thermal fault, T<300Â°C' });
    } else if (R2 <= 0.1 && R1 >= 1 && R5 >= 1 && R5 < 4) {
        this.setState({ basicGasDiagnoText: 'T2: Thermal fault, 300Â°C<T<700Â°C' });
    } else if (R2 <= 0.2 && R1 >= 1 && R5 >= 1 && R5 >= 4) {
        this.setState({ basicGasDiagnoText: 'T3: Thermal fault, T>700Â°C' });
    } else {
        this.setState({ basicGasDiagnoText: 'No clear fault diagnosis possible' });
      }
    if ((C2H4 > theta) && (H2 > theta) && (C2H6 > theta)) {
        this.setState({ gasqoetient1: `C2H2 / C2H4 = ${R2}` });
        this.setState({ gasquotient2: `CH4 / H2 = ${R1}` });
        this.setState({ gasquotient3: `C2H4 / C2H6 = ${R5}` });
      } else {
      this.setState({ gasqoetient1: 'C2H2 / C2H4 = -' });
      this.setState({ gasquotient2: 'CH4 / H2 = -' });
      this.setState({ gasquotient3: 'C2H4 / C2H6 = -' });
      this.setState({ basicGasDiagnoText: 'No fault diagnosis possible' });
    }

// CO2CO01

    if (co2co <= 3 && co2co > 0) {
      this.setState({ co2DiagnoText: 'Recommended to perform a Furan-Analysis or measurement of polymerization with paper samples if possible' });
    } else if (co2co > 3) {
      this.setState({ co2DiagnoText: 'No system fault is implied.' });
    } else {
      this.setState({ co2DiagnoText: 'No clear diagnosis possible' });
  }
    if (CO > theta) {
      this.setState({ co2Ratio: `CO2 / CO = ${co2co}` });
    } else {
        this.setState({ co2Ratio: 'CO2 / CO = -' });
        this.setState({ co2DiagnoText: 'No diagnosis possible' });
    }
//Duval
    if (C2H2 < 0 || C2H4 < 0 || CH4 < 0) {
        this.setState({ duvalDiagnoText: 'No clear diagnosis possible' });
    } else if (CH4Percentage > 98) {
        this.setState({ duvalDiagnoText: 'PD: Partial discharges' });
    } else if (C2H4Percentage <= 23 && C2H2Percentage >= 13) {
        this.setState({ duvalDiagnoText: 'D1: Discharges of low energy' });
    } else if ((C2H4Percentage > 23 && C2H4Percentage <= 40 && C2H2Percentage >= 13) || (C2H4Percentage >= 40 && C2H2Percentage >= 29)) {
        this.setState({ duvalDiagnoText: 'D2: Discharges of high energy' });
    } else if (C2H2Percentage < 4 && C2H4Percentage < 20) {
        this.setState({ duvalDiagnoText: 'T1: Thermal fault, T<300Â°C' });
    } else if (C2H2Percentage < 4 && C2H4Percentage >= 20 && C2H4Percentage < 50) {
        this.setState({ duvalDiagnoText: 'T2: Thermal fault, 300Â°C<T<700Â°C' });
    } else if (C2H2Percentage < 15 && C2H4Percentage >= 50) {
        this.setState({ duvalDiagnoText: 'T3: Thermal fault, T>700Â°C' });
    } else if ((!((C2H2Percentage >= 0) && (C2H2Percentage <= 100))) || (!((C2H4Percentage >= 0) && (C2H4Percentage <= 100))) || (!((CH4Percentage >= 0) && (CH4Percentage <= 100)))) {
        this.setState({ duvalDiagnoText: 'No clear diagnosis possible' });
    } else {
        this.setState({ duvalDiagnoText: 'D+T: Combination of thermal and electrical fault' });
    }

    if ((C2H2 + C2H4 + CH4) > theta) {
      this.setState({ duval1: `C2H2 = ${C2H2Percentage}%` });
      this.setState({ duval2: `C2H4 = ${C2H4Percentage}%` });
      this.setState({ duval3: `CH4 = ${CH4Percentage}%` });
    } else {
      this.setState({ duval1: 'C2H2 = -' });
      this.setState({ duval1: 'C2H4 = -' });
      this.setState({ duval1: 'CH4 = -' });
      this.setState({ duvalDiagnoText: 'No diagnosis possible' });
    }
  }

  render() {
    return (
      <ScrollView style={styles.viewStyle}>
        <Text style={styles.headerText}>Input your values :</Text>
        <View
         style={styles.inputContainer}
        >

          <View style={styles.inputWrapper}>
            <Hoshi
              label={'H2 (Hydrogen)'}
              keyboardType={'numeric'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              onChangeText={(h2Val) => this.setState({ h2Val })}
              value={this.state.h2Val}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Hoshi
              label={'CH4 (Methane)'}
              keyboardType={'numeric'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              onChangeText={(ch4Val) => this.setState({ ch4Val })}
              value={this.state.ch4Val}
            />
          </View>
        </View>

        <View
         style={styles.inputContainer}
        >
          <View style={styles.inputWrapper}>
            <Hoshi
              label={'C2H4 (Ethylene)'}
              keyboardType={'numeric'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              onChangeText={(c2h4Val) => this.setState({ c2h4Val })}
              value={this.state.c2h4Val}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Hoshi
              label={'C2H2 (Acetylene)'}
              keyboardType={'numeric'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              onChangeText={(c2h2Val) => this.setState({ c2h2Val })}
              value={this.state.c2h2Val}
            />
          </View>
        </View>

        <View
         style={styles.inputContainer}
        >
          <View style={styles.inputWrapper}>
            <Hoshi
              label={'C2H6 (Ethane)'}
              keyboardType={'numeric'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              onChangeText={(c2h6Val) => this.setState({ c2h6Val })}
              value={this.state.c2h6Val}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Hoshi
              label={'CO2 (Carbon Dioxide)'}
              keyboardType={'numeric'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              onChangeText={(co2Val) => this.setState({ co2Val })}
              value={this.state.co2Val}
            />
          </View>
        </View>

        <View
         style={styles.inputContainer}
        >
          <View style={styles.inputWrapper}>
          <Hoshi
            label={'N2 (Nitrogen)'}
            keyboardType={'numeric'}
            // this is used as active and passive border color
            borderColor={'#fff'}
            labelStyle={{ color: '#fff', fontSize: 12 }}
            inputStyle={{ color: '#fff' }}
            onChangeText={(n2Val) => this.setState({ n2Val })}
            value={this.state.n2Val}
          />
          </View>
          <View style={{ flex: 1 }}>
            <Hoshi
              label={'O2 (Oxygen)'}
              keyboardType={'numeric'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              onChangeText={(o2Val) => this.setState({ o2Val })}
              value={this.state.o2Val}
            />
          </View>
        </View>

        <Hoshi
          label={'CO (Carbon Monoxide)'}
          // this is used as active and passive border color
          borderColor={'#fff'}
          keyboardType={'numeric'}
          labelStyle={{ color: '#fff', fontSize: 12 }}
          inputStyle={{ color: '#fff' }}
          onChangeText={(coVal) => this.setState({ coVal })}
          value={this.state.coVal}
        />

        <DgaResult state={this.state} />

        <View style={{ marginTop: 30, marginBottom: 20 }}>
          <TouchableOpacity style={[styles.button, { borderColor: 'green' }]} onPress={this.onButtonPress.bind(this)} >
            <Text style={styles.buttonText}>Calculate</Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginBottom: 100 }}>
          <TouchableOpacity style={[styles.button, { borderColor: 'red' }]} onPress={this.onPressClear.bind(this)} >
            <Text style={styles.buttonText}>Clear</Text>
          </TouchableOpacity>
        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    paddingTop: 65,
    paddingLeft: 20,
    paddingRight: 20
  },
  headerText: {
    color: '#fff',
    fontSize: 20,
    marginTop: 20,
    fontWeight: 'bold',
    marginBottom: 15
  },
  inputWrapper: {
    flex: 1,
    marginLeft: 0,
    marginRight: 22
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  button: {
    borderRadius: 3,
    width: WIDTH - 40,
    borderWidth: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff',
    fontSize: 15
  }
});

export default DgaCalculator;
