import React from 'react';
import { Text, View } from 'react-native';

const DgaResult = (props) => {
  const state = props.state;
  if (state.tdcgText !== '') {
    return (
      <View style={{ marginTop: 20 }}>
              <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold', paddingBottom: 10 }}>
                DGA REULTS
              </Text>
              <Text style={styles.lightText}>{state.tdcgText}</Text>
              <View style={styles.dataContainer}>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                  Ratios according to Rogers (IEEE C57.104)
                </Text>
                <Text style={styles.lightText}>{state.rogerDiagnoText}</Text>
                <Text style={styles.lightText}>{state.rogerRatio1}</Text>
                <Text style={styles.lightText}>{state.rogerRatio2}</Text>
                <Text style={styles.lightText}>{state.rogerRatio3}</Text>
              </View>
              <View style={styles.dataContainer}>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                  Ratios according to Doernenburg (IEEE C57.104)
                </Text>
                <Text style={styles.lightText}>{state.doernenDiagnoText}</Text>
                <Text style={styles.lightText}>{state.doernenRatio1}</Text>
                <Text style={styles.lightText}>{state.doernenRatio2}</Text>
                <Text style={styles.lightText}>{state.doernenRatio3}</Text>
                <Text style={styles.lightText}>{state.doernenRatio4}</Text>
              </View>
              <View style={styles.dataContainer}>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                  Ratios according to Basic Gas Ratios (IEC 60599)
                </Text>
                <Text style={styles.lightText}>{state.basicGasDiagnoText}</Text>
                <Text style={styles.lightText}>{state.gasqoetient1}</Text>
                <Text style={styles.lightText}>{state.gasquotient2}</Text>
                <Text style={styles.lightText}>{state.gasquotient3}</Text>
              </View>
              <View style={styles.dataContainer}>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                  CO2 / CO Ratio Analysis according to IEC 60599
                </Text>
                <Text style={styles.lightText}>{state.co2DiagnoText}</Text>
                <Text style={styles.lightText}>{state.co2Ratio}</Text>
              </View>
              <View style={styles.dataContainer}>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                  Duval Diagnosis
                </Text>
                <Text style={styles.lightText}>{state.duval1}</Text>
                <Text style={styles.lightText}>{state.duval2}</Text>
                <Text style={styles.lightText}>{state.duval3}</Text>
              </View>
      </View>
    );
  }
  return <View />;
};

const styles = {
  dataContainer: {
    marginTop: 10
  },
  lightText: {
    color: '#fff',
    fontSize: 14,
    paddingBottom: 5
  }
};
export default DgaResult;
