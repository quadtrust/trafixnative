import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getDiagnostics } from '../actions';
import { BACKGROUND_COLOR, YELLOW } from '../colors/base';

// "name": "IVTR1",
//     "TDCG": 738,
//     "Rogers_C2H2_C2H4": 1,
//     "Rogers_CH4_H2": 1,
//     "Rogers_C2H4_C2H6": 1,
//     "RogersRecommendation": "No clear diagnosis possible",
//     "Doernenburg_C2H2_C2H4": 1,
//     "Doernenburg_CH4_H2": 1,
//     "Doernenburg_C2H2_CH4": 1,
//     "Doernenburg_C2H6_C2H2": 1,
//     "DoernenburgRecommendation": "No clear diagnosis possible",
//     "BasisGasquotienten_C2H2_C2H4": 1,
//     "BasisGasquotienten_CH4_H2": 1,
//     "BasisGasquotienten_C2H4_C2H6": 1,
//     "BasisGasquotientenRecommendation": "No clear fault diagnosis possible",
//     "CO2CO_Diagnosis": 1,
//     "CO2CO_DiagnosisRecommendation": "Recommended to perform a Furan- if possible",
//     "Duval_C2H2": 33,
//     "Duval_C2H4": 33,
//     "Duval_CH4": 33,
//     "DuvalRecommendation": "No clear diagnosis possible"

const WIDTH = Dimensions.get('window').width;

class Diagnostics extends Component {
  componentWillMount() {
    this.props.getDiagnostics();
  }
  render() {
    console.log(this.props.diagnostics[0].BasisGasquotientenRecommendation);
    return (
      <ScrollView style={styles.viewStyle}>
        <View style={{ backgroundColor: 'rgba(0, 0, 0, .1)', padding: 7, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 20, alignSelf: 'center' }}>
            {this.props.diagnostics[0].name}
          </Text>
          <Text style={styles.textStyle}>
            TDCG :
            {this.props.diagnostics[0].TDCG}
          </Text>
        </View>

        <View style={styles.cardContainer}>
          <Text style={styles.reportHeading}>Rogers Diagnostics Report :</Text>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.dataHeading}>Ratio of Gas</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.dataHeading}>Value</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>C2H2/C2H4</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].Rogers_C2H2_C2H4}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>CH4/H2</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].Rogers_CH4_H2}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>C2H4/C2H6</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].Rogers_C2H4_C2H6}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.resultText}>Result : </Text>
            <Text style={styles.textStyle}>
              {this.props.diagnostics[0].RogersRecommendation}
            </Text>
          </View>
        </View>

        <View style={styles.cardContainer}>
          <Text style={styles.reportHeading}>Doernenburg Diagnostics Report :</Text>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.dataHeading}>Ratio of Gas</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.dataHeading}>Value</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>C2H2/C2H4</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].Doernenburg_C2H2_C2H4}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>CH4/H2</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].Doernenburg_CH4_H2}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>C2H4/CH4</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].Doernenburg_C2H2_CH4}
              </Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>C2H6/C2H2</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].Doernenburg_C2H6_C2H2}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.resultText}>Result : </Text>
            <Text style={styles.textStyle}>
              {this.props.diagnostics[0].DoernenburgRecommendation}
            </Text>
          </View>
        </View>

        <View style={styles.cardContainer}>
          <Text style={styles.reportHeading}>Basic Gas Quotient Diagnostics Report :</Text>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.dataHeading}>Ratio of Gas</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.dataHeading}>Value</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>CH4/CH2</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].BasisGasquotienten_CH4_H2}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>C2H2/C2H4</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].BasisGasquotienten_C2H2_C2H4}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>C2H4/C2H6</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].BasisGasquotienten_C2H4_C2H6}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.resultText}>Result : </Text>
            <Text style={styles.textStyle}>
              {this.props.diagnostics[0].BasisGasquotientenRecommendation}
            </Text>
          </View>
        </View>

        <View style={styles.cardContainer}>
          <Text style={styles.reportHeading}>CO2 / CO Diagnostics Report :</Text>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.dataHeading}>Ratio of Gas</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.dataHeading}>Value</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>CO2/CO</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].CO2CO_Diagnosis}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.resultText}>Result : </Text>
            <Text style={styles.textStyle}>
              {this.props.diagnostics[0].CO2CO_DiagnosisRecommendation}
            </Text>
          </View>
        </View>

        <View style={styles.cardContainer}>
          <Text style={styles.reportHeading}>Duval Diagnostics Report :</Text>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>Duval CH4</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].Duval_CH4}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>Duval C2H4</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].Duval_C2H2}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>Duval C2H4</Text>
            </View>
            <View style={styles.dataWrapper}>
              <Text style={styles.textStyle}>
                {this.props.diagnostics[0].Duval_C2H4}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.resultText}>Result : </Text>
            <Text style={styles.textStyle}>
              {this.props.diagnostics[0].DuvalRecommendation}
            </Text>
          </View>
        </View>
        <View style={{ marginTop: 20, marginBottom: 50 }}>
          <TouchableOpacity
            style={[styles.button, { borderColor: 'green' }]}
          >
            <Text style={styles.buttonText}>Download Reports</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  viewStyle: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
    paddingBottom: 50,
  },
  textStyle: {
    color: '#fff',
    fontSize: 14,
    paddingTop: 4,
    paddingBottom: 4
  },
  dataWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff'
  },
  cardContainer: {
    marginTop: 5,
    marginBottom: 5
  },
  dataHeading: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold'
  },
  reportHeading: {
    color: '#fff',
    fontSize: 17,
    fontWeight: 'bold',
    paddingBottom: 10
  },
  resultText: {
    color: YELLOW,
    alignSelf: 'flex-start',
    paddingTop: 3
  },
  button: {
    borderRadius: 3,
    width: WIDTH - 40,
    borderWidth: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff',
    fontSize: 15
  }
};

const mapStateToProps = state => {
  return {
    diagnostics: _.values(state.samples.diagnostics)
  };
};

export default connect(mapStateToProps, { getDiagnostics })(Diagnostics);
