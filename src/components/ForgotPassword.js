import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions, TouchableOpacity } from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Jiro } from 'react-native-textinput-effects';
import { BACKGROUND_COLOR, BUTTON_GREY, YELLOW } from '../colors/base';

const WIDTH = Dimensions.get('window').width;

class ForgotPassword extends Component {
  render() {
    return (
      <ScrollView style={{ backgroundColor: BACKGROUND_COLOR }}>
      <View style={styles.forgotPasswordContainer}>
        <View style={styles.forgotPasswordInfo}>
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}
          >
            <View>
              <Text style={{ fontSize: 22, color: '#fff', fontWeight: 'bold' }}>
               Forgot Password</Text>
              <Text style={styles.ForgotPasswordText}>
               Enter your email address to get password:
              </Text>
            </View>
            <FontAwesomeIcon
              name='lock'
              size={35}
              color="#999"
              style={{
                alignSelf: 'flex-start',
                  }}
            />
          </View>
          <View style={{ marginTop: 20 }}>

            <Jiro
              label={'Email Address'}
              // this is used as active and passive border color
              labelStyle={{ color: YELLOW, fontSize: 16 }}
              borderColor={'#fff'}
              inputStyle={{ color: '#000' }}
            />

            <View style={{ marginTop: 20 }}>
              <TouchableOpacity style={[styles.button, { backgroundColor: BUTTON_GREY }]}>
                <Text style={styles.buttonText}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  forgotPasswordContainer: {
    flex: 1,
    padding: 10
  },
  forgotPasswordInfo: {
    borderColor: BUTTON_GREY,
    borderWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 20,
    paddingTop: 20,
    marginTop: 50,
    marginBottom: 20,
    backgroundColor: 'rgba(0,0,0,.1)'
  },
  ForgotPasswordText: {
    color: '#fff',
    fontSize: 14,
    paddingBottom: 5,
    paddingTop: 5
  },
  input: {
    height: 50
  },
  button: {
    borderRadius: 3,
    width: WIDTH - 40,
    borderWidth: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff',
    fontSize: 15
  }
};

export default ForgotPassword;
