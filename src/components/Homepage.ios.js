/*
eslint max-len : 0
*/
import React, { Component } from 'react';
import {
  View, StyleSheet, Image,
  Text, Platform, StatusBar,
  ScrollView, Dimensions
} from 'react-native';
import { YELLOW } from '../colors/base';

import RowView from './common/RowView';
import MenuItem from './common/MenuItem';

const SCREEN_WIDTH = Dimensions.get('window').width;

class Homepage extends Component {

  render() {
    console.log(this.props.navigation);
    return (
      <ScrollView>
      <View style={styles.pageStyle}>
        <StatusBar
          backgroundColor={YELLOW}
          barStyle="light-content"
        />
        {/* <TouchableWithoutFeedback onPress={() => { console.log('test!! '); }}>
          <Icon
            name='ios-power'
            size={45}
            color="#fff"
            style={{
              alignSelf: 'flex-end',
              marginTop: 10,
              marginRight: 20,
              marginBottom: -10
            }}
          />
        </TouchableWithoutFeedback> */}
        <Image
          style={styles.mainImage}
          source={require('../assets/images/login_logo.png')}
        />
        <View>

          <Text
            style={{
              color: '#fff',
              marginTop: (Platform.OS === 'android') ? 10 : -30,
              marginBottom: 10,
              fontSize: 10,
              textAlign: 'center'
            }}
          >Trafix v2.0
          </Text>

          <RowView>
            <MenuItem icon={'search'} title={'Assets'} RouterKey={'assetsList'} navigation={this.props.navigation} />
            <MenuItem icon={'calculator'} title={'DGA Calculator'} RouterKey={'dgaCalculator'} navigation={this.props.navigation} />
            <MenuItem icon={'bell'} title={'Alarm Sets'} RouterKey={'alarmSets'} navigation={this.props.navigation} />
            <MenuItem icon={'sitemap'} title={'Zones'} RouterKey={'zoneList'} navigation={this.props.navigation} />
            <MenuItem icon={'heartbeat'} title={'Critical Assets'} RouterKey={'criticalAssets'} navigation={this.props.navigation} />
            <MenuItem icon={'plus-square'} title={'Add Substation'} RouterKey={'addSubstation'} navigation={this.props.navigation} />
            <MenuItem icon={'user'} title={'Profile'} RouterKey={'profilePage'} navigation={this.props.navigation} />
          </RowView>
        </View>

        <View>
          <Image
            style={styles.connectingImage}
            source={require('../assets/images/connecting-line.png')}
          />
        </View>
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  pageStyle: {
    flex: 1,
    backgroundColor: '#282c34',
    justifyContent: 'space-between'
  },
  mainImage: {
    width: 270,
    height: 52,
    marginTop: 50,
    marginBottom: 50,
    alignSelf: 'center'
  },
  connectingImage: {
    width: SCREEN_WIDTH,
    height: 70,
    resizeMode: 'contain'
  }
});

export default Homepage;
