import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  LayoutAnimation,
  Image,
  TouchableWithoutFeedback,
  StatusBar
} from 'react-native';
import { Madoka } from 'react-native-textinput-effects';
import Button from 'react-native-button';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { emailChanged, passwordChanged, loginUser } from '../actions';
import { BACKGROUND_COLOR } from '../colors/base';

import Spinner from './common/Spinner';

class LoginForm extends Component {
  
  // Putting up some basic animations
  componentWillUpdate() {
    LayoutAnimation.easeInEaseOut();
  }
  onEmailChange(text) {
    this.props.emailChanged(text);
  }
  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }
  onButtonPress() {
    const { email, password } = this.props;
    this.props.loginUser({ email, password });
    // this.props.navigation.navigate('homePage');
  }
  renderError() {
    if (this.props.error !== '') {
      return (
        <Text style={styles.errorText}>{this.props.error}</Text>
      );
    }
  }
  renderButton() {
    if (this.props.spinner) {
      return (
        <Spinner />
      );
    }
      return (
        <Button
          containerStyle={{
              padding: 10,
              height: 45,
              overflow: 'hidden',
              borderRadius: 5,
              backgroundColor: '#51597d'
            }}
          style={{ fontSize: 20, color: 'white' }}
          onPress={this.onButtonPress.bind(this)}
        >
        Login
        </Button>
      );
  }
  render() {
    return (

        <View style={styles.viewStyle}>
          <StatusBar
            backgroundColor='#32353e'
            barStyle="light-content"
          />
          <View>
            <Image
              style={styles.headerImage}
              source={require('../assets/images/trafixheader.png')}
            />
            {this.renderError()}
            <Madoka
              style={styles.input}
              label={'Username'}
              borderColor={'#f8f8f8'}
              labelStyle={{ color: '#fff' }}
              inputStyle={{ color: '#fff' }}
              onChangeText={this.onEmailChange.bind(this)}
              value={this.props.email}
            />
            <Madoka
              style={styles.input}
              label={'Password'}
              borderColor={'#f8f8f8'}
              labelStyle={{ color: '#fff' }}
              inputStyle={{ color: '#fff' }}
              secureTextEntry
              onChangeText={this.onPasswordChange.bind(this)}
              value={this.props.password}
            />
          {this.renderButton()}
          </View>
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('forgotPassword')}>
            <View>
              <Text
                style={{
                    color: '#fff',
                    marginTop: 30,
                    alignSelf: 'center'
                  }}
              >Forgot Password?</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    padding: 20,
    flex: 1,
    justifyContent: 'center',
    paddingBottom: 100,
    backgroundColor: '#32353e'
  },
  errorText: {
    color: '#cc3333',
    textAlign: 'center',
    fontSize: 15,
    padding: 10
  },
  headerImage: {
    height: 180,
    width: 250,
    alignSelf: 'center',
    marginBottom: 10
  }
});

const mapStateToProps = state => {
  return {
    email: state.auth.email,
    password: state.auth.password,
    error: state.auth.error,
    spinner: state.auth.spinner
  };
};
export default connect(mapStateToProps, { emailChanged, passwordChanged, loginUser })(LoginForm);
