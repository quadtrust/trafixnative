import React, { Component } from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';
import { connect } from 'react-redux';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { BACKGROUND_COLOR } from '../colors/base';
import { profileFetch } from '../actions';

class ProfilePage extends Component {
  componentWillMount() {
    // Load the profile info.
    this.props.profileFetch();
  }
  render() {
    console.log('Profile Props: ', this.props.profile);
    console.log('Pic Link: ', `https://${this.props.profile.profile_pic}`);
      return (
        <View style={{ backgroundColor: BACKGROUND_COLOR, flex: 1, padding: 10 }}>
          <View style={styles.profilePageConatiner}>
            <Image
            style={styles.imageStyle}
            source={{ uri: `https://${this.props.profile.profile_pic}` }}
            />
            <Text style={styles.profileInfoText}>
              {this.props.profile.name}
            </Text>
            <Text style={{ color: '#999' }}>
              {this.props.user.email}
            </Text>
          </View>

          <View style={styles.dividerStyle} />
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ color: '#fff', fontWeight: 'bold' }}>Contact No. :</Text>
            <View>
              <Text style={styles.profileContentText}>{this.props.profile.mobile_number}</Text>
            </View>
          </View>

          <View style={styles.dividerStyle} />

          <View style={{ flexDirection: 'row' }}>
            <Text style={{ color: '#fff', fontWeight: 'bold' }}>Designation :</Text>
            <View>
              <Text style={styles.profileContentText}>{this.props.profile.designation}</Text>
            </View>
          </View>

          <View style={styles.dividerStyle} />

          <View style={{ flexDirection: 'row' }}>
            <Text style={{ color: '#fff', fontWeight: 'bold' }}>Department :</Text>
            <View>
              <Text style={styles.profileContentText}>{this.props.profile.department}</Text>
            </View>
          </View>

          <View style={styles.dividerStyle} />

          <View style={{ flexDirection: 'row' }}>
            <Text style={{ color: '#fff', fontWeight: 'bold' }}>No. of DGA Entries :</Text>
            <View>
              <Text style={{ color: '#fff', paddingLeft: 5 }}>
                {this.props.profile.total_assets}
              </Text>
            </View>
          </View>

          <View style={styles.dividerStyle} />

          {/* <Content style={{ marginTop: 20, marginBottom: 100 }}>
            <Button info bordered block>
              <NBText>Edit Profile</NBText>
            </Button>
          </Content> */}
        </View>
      );
  }
}

const styles = StyleSheet.create({
  profilePageConatiner: {
    marginTop: 20,
    marginBottom: 10,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  imageStyle: {
    width: 120,
    height: 120,
    borderRadius: 60,
    borderWidth: 5,
    borderColor: '#fff'
  },
  profileInfoText: {
    color: '#fff',
    fontSize: 20,
    paddingTop: 5
  },
  dividerStyle: {
    height: 0.7,
    backgroundColor: '#999',
    marginTop: 8,
    marginBottom: 8
  },
  profileContentText: {
    color: '#fff',
    paddingLeft: 5
  }
});

const mapStateToProps = state => {
  return {
    profile: state.profile.profile,
    user: state.profile.user
  };
};

export default connect(mapStateToProps, { profileFetch })(ProfilePage);
