import React from 'react';
import { View, Text } from 'react-native';

const Results = (props) => {
  const state = props.state;
  if (state.greaterText !== '') {
    return (
      <View>
        <Text>{state.greaterText}</Text>
        <Text>Ratio: {state.h1 / state.h2} </Text>
      </View>
    );
  }
  return null;
};

export default Results;
