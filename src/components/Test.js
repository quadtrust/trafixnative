/*global navigator:false*/

import React, { Component } from 'react';
import { View } from 'react-native';

class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0
    };
  }
  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log(position);
        console.log('Latitude: ', position.coords.latitude);
        console.log('Logitude: ', position.coords.longitude);
      },
      (error) => console.log(JSON.stringify(error))
    );
  }
  render() {
    return (
      <View />
    );
  }
}

export default Test;
