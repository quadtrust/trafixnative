import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
// import Icon from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import { connect } from 'react-redux';
import { fetchZones } from '../actions';
import { BACKGROUND_COLOR, BUTTON_GREY, RGBA_RED, RGBA_GREEN } from '../colors/base';

class ZoneList extends Component {
  componentWillMount() {
    this.props.fetchZones();
  }

  healthOfZone(health) {
    if ((health === 1) || (health === 2)) {
      return 'Good';
    } else if ((health === 3) || (health === 4)) {
      return 'Bad';
    }
    return '-';
  }

  zoneHealthColor(health) {
    if ((health === 1) || (health === 2)) {
      return (
        styles.listHealthColorGreen
      );
    } else if ((health === 3) || (health === 4)) {
      return (
        styles.listHealthColor
      );
    }
    return null;
  }

  renderZones() {
    return this.props.zones.map(zone =>
      <TouchableOpacity onPress={() => console.log(zone.id)} key={zone.id}>
        <View style={[styles.ZonetListStyle, zone.no_of_asset === 0 ? null : this.zoneHealthColor(zone.average_health)]}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ justifyContent: 'space-between', marginLeft: 20 }}>
              <Text style={styles.textColor}>{zone.name}</Text>
              <Text style={styles.textColor}>No. of Assets : {zone.no_of_asset} </Text>
              <Text style={{ fontSize: 13, color: '#CCC' }}>
                Health of Zone :
                { zone.no_of_asset === 0 ? ' -' : this.healthOfZone(zone.average_health)}
              </Text>
            </View>
          </View>
          {/* <Icon
            name='ios-arrow-forward'
            size={45}
            color="#eee"
            style={{
              alignSelf: 'flex-end'
            }}
          /> */}
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    console.log(this.props.zones);
    return (
      <ScrollView
        style={{
          backgroundColor: BACKGROUND_COLOR,
          flex: 1,
          paddingTop: 20,
          paddingLeft: 10,
          paddingRight: 10,
        }}
      >
      <View style={{ marginBottom: 30 }}>
        {this.renderZones()}
      </View>
    </ScrollView>
    );
  }
}

const styles = {
  ZonetListStyle: {
    marginBottom: 10,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: BUTTON_GREY,
  },
  listHealthColor: {
    borderLeftWidth: 7,
    borderColor: RGBA_RED
  },
  listHealthColorGreen: {
    borderLeftWidth: 7,
    borderColor: RGBA_GREEN
  },
  textColor: {
    color: '#CCC'
  }
};

const mapStateToProps = state => {
  return {
    zones: _.values(state.zones.zones)
  };
};

export default connect(mapStateToProps, { fetchZones })(ZoneList);
