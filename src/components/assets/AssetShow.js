import React, { Component } from 'react';
import _ from 'lodash';
import { ScrollView, View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

import { assetFetch, getReport } from '../../actions/';
import { BACKGROUND_COLOR } from '../../colors/base';
import BigHealthImage from '../common/BigHealthImage';
import { MAIN_LINK } from '../../actions/api';
import Spinner from '../common/Spinner';

const WIDTH = Dimensions.get('window').width;

class AssetShow extends Component {
  componentWillMount() {
    this.props.assetFetch();
    // this.props.asset[0].latitude = 0;
    // this.props.asset[0].longitude = 0;
  }

  renderMap(location) {
    if (location) {
      return (
        <MapView
          style={{ width: (Dimensions.get('window') - 10), height: 200 }}
          provider={PROVIDER_GOOGLE}
          // customMapStyle={mapStyle}
          initialRegion={{
            latitude: this.props.asset[0].latitude || 0,
            longitude: this.props.asset[0].longitude || 0,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        >
          <MapView.Marker
           coordinate={{
             latitude: this.props.asset[0].latitude || 0,
             longitude: this.props.asset[0].longitude || 0 }}
           title={'Test'}
           description={'Something else'}
          />
        </MapView>
      );
    }
  }

  render() {
    console.log(this.props.navigation.state.key);
    console.log('Props: ', this.props.asset[0]);
    console.log('Dimensions: ', Dimensions.get('window'));
    console.log(`${MAIN_LINK}en/assets/${this.props.asset[0].id}.pdf`);
    if (this.props.asset[0].length < 1) {
      return (
      <View style={styles.spinnerContainer}>
        <Spinner />
      </View>);
    }
    return (
      <ScrollView style={styles.viewStyle}>
        <View style={{ paddingTop: 30, alignSelf: 'center', paddingBottom: 10 }}>
          <BigHealthImage health={this.props.asset[0].health} />
      {/* <HealthImage health={this.props.asset[0].health} size={200} /> */}
        </View>

        <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
          <Text style={{ color: '#fff', fontSize: 20 }}>Name: {this.props.asset[0].name}</Text>
          <Text style={{ paddingLeft: 5, color: '#999', fontSize: 13 }}>
            ({this.props.asset[0].alarm_type})
          </Text>
        </View>
        <View style={{ height: 0.7, backgroundColor: '#fff', marginTop: 5, marginBottom: 5 }} />
        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>
              Serial Number :
            </Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>
              {this.props.asset[0].serial_number}
            </Text>
          </View>
        </View>

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Location : </Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].location} </Text>
          </View>
        </View>

        {/*<View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Kilo Volt :</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].kv}</Text>
          </View>
        </View>*/}

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Oil Type :</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].oil_type}</Text>
          </View>
        </View>

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Paper Type :</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].paper_type}</Text>
          </View>
        </View>

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Oil Volume :</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].oil_volume}</Text>
          </View>
        </View>

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Manufacturer :</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].equipment_manufacturer}</Text>
          </View>
        </View>

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Mega Volt Ampere  :</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].mva}</Text>
          </View>
        </View>

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Year of Manufacturer :</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].year_of_manufacture}</Text>
          </View>
        </View>

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Asset Health :</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].health}</Text>
          </View>
        </View>

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Paper Quantity :</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].paper_quantity}</Text>
          </View>
        </View>

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>OP System:</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].oil_preservation_system}</Text>
          </View>
        </View>

        <View style={styles.assetDetailsContainer}>
          <View style={styles.assetTextWrapper}>
            <Text style={styles.assetInfoText}>Decription :</Text>
          </View>
          <View style={styles.assetTextWrapperLeft}>
            <Text style={styles.assetInfoText}>{this.props.asset[0].description}</Text>
          </View>
        </View>


        {this.renderMap(this.props.asset[0].latitude)}

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>

        <View style={{ marginTop: 20 }}>
          <TouchableOpacity
            style={[styles.button, { borderColor: 'yellow' }]}
            onPress={() => {
            // Actions.samplesList();
            this.props.navigation.navigate('samplesList');
          }}
          >
            <Text style={styles.buttonText}>View Samples</Text>
          </TouchableOpacity>
        </View>
        </View>
        <View style={{ marginTop: 20, marginBottom: 100 }}>
          <TouchableOpacity
            style={[styles.button, { borderColor: 'orange' }]}
            onPress={() => { this.props.getReport(); }}
          >
            <Text style={styles.buttonText}>Download Reports</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  console.log(_.values(state.assets.singleAsset));
  return {
    asset: _.values(state.assets.singleAsset)
  };
};

const styles = StyleSheet.create({
  viewStyle: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    padding: 10
  },
  assetDetailsContainer: {
    // marginBottom: 1,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#999',
    marginBottom: 5
  },
  assetTextWrapper: {
    flex: 1,
    // alignItems: 'center',
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  assetTextWrapperLeft: {
    flex: 1,
    // alignItems: 'center',
    borderLeftWidth: 1,
    borderColor: '#999',
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center'

  },
  borderStyling: {
    borderColor: '#999',
    borderLeftWidth: 1
  },
  assetInfoText: {
    color: '#fff',
    fontSize: 15
  },
  button: {
    borderRadius: 3,
    width: WIDTH - 20,
    borderWidth: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff'
  },
  spinnerContainer: {
    backgroundColor: BACKGROUND_COLOR, 
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },

});

export default connect(mapStateToProps, { assetFetch, getReport })(AssetShow);
