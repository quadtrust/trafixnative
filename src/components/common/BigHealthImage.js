import React from 'react';
import { Image } from 'react-native';

const BigHealthImage = (props) => {
  if (props.health === 1) {
    return (
      <Image
        style={{ height: 180, width: 180 }}
        source={require('../../assets/images/trans_green.png')}
      />
    );
  } else if (props.health === 2) {
    return (
      <Image
        style={{ height: 180, width: 180 }}
        source={require('../../assets/images/trans_orange.png')}
      />
    );
  } else if (props.health === 3) {
    return (
      <Image
        style={{ height: 180, width: 180 }}
        source={require('../../assets/images/trans_red.png')}
      />
    );
  } else if (props.health === 4) {
    return (
      <Image
        style={{ height: 180, width: 180 }}
        source={require('../../assets/images/trans_red.png')}
      />
    );
  }

  return (<Image
    style={{ height: 180, width: 180 }}
    source={require('../../assets/images/trans_red.png')}
  />);
};

export default BigHealthImage;
