import React from 'react';
import { View, Text } from 'react-native';
import { HEADER_COLOR } from '../../colors/base';

const Header = (props) => {
    return (
      <View style={styles.viewStyles}>
        <Text style={styles.headerText}>
          {props.headerText}
        </Text>
      </View>
    );
  };

const styles = {
  viewStyles: {
    height: 60,
    backgroundColor: HEADER_COLOR,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10
  },
  headerText: {
    fontSize: 20,
    color: '#ddd',
    fontWeight: '500',
    paddingTop: 15
  }
};

export default Header;
