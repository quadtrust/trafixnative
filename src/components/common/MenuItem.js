import React from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Iconio from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';

const MenuItem = (props) => {
  return (
      <TouchableWithoutFeedback
        // onPress={() => { Actions[props.RouterKey](); }}
        onPress={() => props.navigation.navigate(props.RouterKey)}
      >
        <View style={styles.menuItemStyle}>
          <Icon name={props.icon} size={45} color="#fff" />

          <Text style={[styles.textStyle]} adjustsFontSizeToFit numberOfLines={1}>
            {props.title}
          </Text>

          <Iconio
            name='ios-arrow-forward'
            size={45}
            color="#eee"
            style={{
              alignSelf: 'flex-end'
            }}
          />
        </View>
      </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  menuItemStyle: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    borderRadius: 3,
    backgroundColor: '#42464E',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 40,
    paddingRight: 40,
    margin: 3
  },
  textStyle: {
    fontSize: 17,
    color: '#fff',
    alignSelf: 'center'
  }
});

export default MenuItem;
