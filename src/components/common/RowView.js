import React from 'react';
import { View, StyleSheet } from 'react-native';

const RowView = (props) => {
  return (
    <View style={styles.rowViewStyle}>
      {props.children}
    </View>
  );
};

const styles = StyleSheet.create({
  rowViewStyle: {
    padding: 7,
    paddingBottom: 20
  }
});

export default RowView;
