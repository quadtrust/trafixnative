import React, { Component } from 'react';
import { View, StyleSheet, Picker, Text } from 'react-native';
import { Hoshi } from 'react-native-textinput-effects';
import { connect } from 'react-redux';
import DatePicker from 'react-native-datepicker';
import { sampleUpdate } from '../../actions';

class SampleForm extends Component {
  render() {
    return (
    <View>
      <View>
           <View>
            <Text style={{ color: '#fff', marginTop: 7 }}>Select the Source Type :</Text>
            <Picker
              selectedValue={this.props.source_type}
              onValueChange={(value) => this.props.sampleUpdate({ prop: 'source_type', value })}
            >
              <Picker.Item label="Field Test" value="1" />
              <Picker.Item label="Lab Test" value="0" />
            </Picker>
          </View>
        <View style={styles.inputContainer}>
          <View style={styles.inputWrapper}>
            <DatePicker
              style={{ width: 150, marginTop: 15 }}
              date={this.props.sample_date}
              mode="date"
              placeholder="select date"
              format="YYYY-MM-DD"
              minDate="2016-05-01"
              maxDate="2017-06-01"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36
                },
                dateText: {
                  color: '#fff'
                }
                // ... You can check the source to find the other keys.
              }}
              onDateChange={(date) => this.props.sampleUpdate({ prop: 'sample_date', value: date })}
            />
          </View>

          <View style={{ flex: 1 }}>
            <Hoshi
              label={'Enter Sample no.'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.sample_number}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'sample_number', value: text })}
            />
          </View>
        </View>

        <View
         style={styles.inputContainer}
        >
        <View style={styles.inputWrapper}>
          <Hoshi
            label={'N2 (Nitrogen)'}
            // this is used as active and passive border color
            borderColor={'#fff'}
            keyboardType={'numeric'}
            labelStyle={{ color: '#fff', fontSize: 12 }}
            inputStyle={{ color: '#fff' }}
            value={this.props.n2}
            onChangeText={(text) => this.props.sampleUpdate({ prop: 'n2', value: text })}
          />
          </View>
          <View style={{ flex: 1 }}>
            <Hoshi
              label={'O2 (Oxygen)'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.o2}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'o2', value: text })}
            />
          </View>
        </View>

        <View
         style={styles.inputContainer}
        >
        <View style={styles.inputWrapper}>
          <Hoshi
            label={'CH4 (Methane)'}
            // this is used as active and passive border color
            borderColor={'#fff'}
            keyboardType={'numeric'}
            labelStyle={{ color: '#fff', fontSize: 12 }}
            inputStyle={{ color: '#fff' }}
            value={this.props.ch4}
            onChangeText={(text) => this.props.sampleUpdate({ prop: 'ch4', value: text })}
          />
          </View>
          <View style={{ flex: 1 }}>
            <Hoshi
              label={'C2H2 (Acetylene)'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.c2h2}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'c2h2', value: text })}
            />
          </View>
        </View>

        <View
         style={styles.inputContainer}
        >
        <View style={styles.inputWrapper}>
          <Hoshi
            label={'C2H4 (Ethylene)'}
            // this is used as active and passive border color
            borderColor={'#fff'}
            keyboardType={'numeric'}
            labelStyle={{ color: '#fff', fontSize: 12 }}
            inputStyle={{ color: '#fff' }}
            value={this.props.c2h4}
            onChangeText={(text) => this.props.sampleUpdate({ prop: 'c2h4', value: text })}
          />
          </View>
          <View style={{ flex: 1 }}>
            <Hoshi
              label={'CO2 (Carbon dioxid)'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.co2}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'co2', value: text })}
            />
          </View>
        </View>

        <View
         style={styles.inputContainer}
        >
        <View style={styles.inputWrapper}>
          <Hoshi
            label={'CO (Carbon monoxide)'}
            // this is used as active and passive border color
            borderColor={'#fff'}
            keyboardType={'numeric'}
            labelStyle={{ color: '#fff', fontSize: 12 }}
            inputStyle={{ color: '#fff' }}
            value={this.props.co}
            onChangeText={(text) => this.props.sampleUpdate({ prop: 'co', value: text })}
          />
          </View>
          <View style={{ flex: 1 }}>
            <Hoshi
              label={'C2H6 (Ethane)'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.c2h6}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'c2h6', value: text })}
            />
          </View>
        </View>


        <View
         style={styles.inputContainer}
        >
        <View style={styles.inputWrapper}>
          <Hoshi
            label={'H2 (Hydrogen)'}
            // this is used as active and passive border color
            borderColor={'#fff'}
            keyboardType={'numeric'}
            labelStyle={{ color: '#fff', fontSize: 12 }}
            inputStyle={{ color: '#fff' }}
            value={this.props.h2}
            onChangeText={(text) => this.props.sampleUpdate({ prop: 'h2', value: text })}
          />
          </View>
          <View style={{ flex: 1 }}>
            <Hoshi
              label={'Value of Moisture'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.moisture}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'moisture', value: text })}
            />
          </View>
        </View>
        </View
        // end of field test input view
        >

  { /* <View >
          <View
           style={styles.inputContainer}
          >
          <View style={styles.inputWrapper}>
            <Hoshi
              label={'Value Of 2-FAL'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.fal}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'fal', value: text })}
            />
            </View>
            <View style={{ flex: 1 }}>
              <Hoshi
                label={'Value Of 2-FOL'}
                // this is used as active and passive border color
                borderColor={'#fff'}
                keyboardType={'numeric'}
                labelStyle={{ color: '#fff', fontSize: 12 }}
                inputStyle={{ color: '#fff' }}
                value={this.props.fol}
                onChangeText={(text) => this.props.sampleUpdate({ prop: 'fol', value: text })}
              />
            </View>
          </View>

          <View
           style={styles.inputContainer}
          >
            <View style={styles.inputWrapper}>
            <Hoshi
              label={'Value Of IFT'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.ift}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'ift', value: text })}
            />
            </View>
            <View style={{ flex: 1 }}>
              <Hoshi
                label={'Value Of 5-MEF'}
                // this is used as active and passive border color
                borderColor={'#fff'}
                keyboardType={'numeric'}
                labelStyle={{ color: '#fff', fontSize: 12 }}
                inputStyle={{ color: '#fff' }}
                value={this.props.mef}
                onChangeText={(text) => this.props.sampleUpdate({ prop: 'mef', value: text })}
              />
            </View>
          </View>

          <View
           style={styles.inputContainer}
          >
            <View style={styles.inputWrapper}>
            <Hoshi
              label={'Value Of D1816-1mm'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.d1816}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'd1816', value: text })}
            />
            </View>
            <View style={{ flex: 1 }}>
              <Hoshi
                label={'Value Of Acid NB'}
                // this is used as active and passive border color
                borderColor={'#fff'}
                keyboardType={'numeric'}
                labelStyle={{ color: '#fff', fontSize: 12 }}
                inputStyle={{ color: '#fff' }}
                value={this.props.acid}
                onChangeText={(text) => this.props.sampleUpdate({ prop: 'acid', value: text })}
              />
            </View>
          </View>

          <View
           style={styles.inputContainer}
          >
            <View style={styles.inputWrapper}>
            <Hoshi
              label={'Value Of 2-ACF'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.acf}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'acf', value: text })}
            />
            </View>
            <View style={{ flex: 1 }}>
              <Hoshi
                label={'Value Of D877'}
                // this is used as active and passive border color
                borderColor={'#fff'}
                keyboardType={'numeric'}
                labelStyle={{ color: '#fff', fontSize: 12 }}
                inputStyle={{ color: '#fff' }}
                value={this.props.d877}
                onChangeText={(text) => this.props.sampleUpdate({ prop: 'd877', value: text })}
              />
            </View>
          </View>

          <View
           style={styles.inputContainer}
          >
            <View style={styles.inputWrapper}>
            <Hoshi
              label={'Value Of 5-HMF'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.hmf}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'hmf', value: text })}
            />
            </View>
            <View style={{ flex: 1 }}>
              <Hoshi
                label={'Value Of D1816-2mm'}
                // this is used as active and passive border color
                borderColor={'#fff'}
                keyboardType={'numeric'}
                labelStyle={{ color: '#fff', fontSize: 12 }}
                inputStyle={{ color: '#fff' }}
                value={this.props.d18162mm}
                onChangeText={(text) => this.props.sampleUpdate({ prop: 'd18162mm', value: text })}
              />
            </View>
          </View>

          <View
           style={styles.inputContainer}
          >
            <View style={styles.inputWrapper}>
            <Hoshi
              label={'Value Of Furan'}
              // this is used as active and passive border color
              borderColor={'#fff'}
              keyboardType={'numeric'}
              labelStyle={{ color: '#fff', fontSize: 12 }}
              inputStyle={{ color: '#fff' }}
              value={this.props.furan}
              onChangeText={(text) => this.props.sampleUpdate({ prop: 'furan', value: text })}
            />
            </View>
            <View style={{ flex: 1 }}>
              <Hoshi
                label={'Value Of Color'}
                // this is used as active and passive border color
                borderColor={'#fff'}
                keyboardType={'numeric'}
                labelStyle={{ color: '#fff', fontSize: 12 }}
                inputStyle={{ color: '#fff' }}
                value={this.props.color}
                onChangeText={(text) => this.props.sampleUpdate({ prop: 'color', value: text })}
              />
            </View>
          </View>
        </View
        //end of Lab Test input View
        >*/ }
   </View>
 );
}
}

const styles = StyleSheet.create({
  headerText: {
    color: '#fff'
  },
  inputWrapper: {
    flex: 1,
    marginLeft: 0,
    marginRight: 7
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
function mapStateToProps(state) {
  const { source_type,
  sample_date,
  sample_number,
  n2,
  o2,
  ch4,
  c2h2,
  c2h4,
  co2,
  co,
  c2h6,
  h2,
  moisture
   } = state.samples;
  return {
    source_type,
    sample_date,
    sample_number,
    n2,
    o2,
    ch4,
    c2h2,
    c2h4,
    co2,
    co,
    c2h6,
    h2,
    moisture
  };
}
export default connect(mapStateToProps, { sampleUpdate })(SampleForm);
