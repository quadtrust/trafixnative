import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import moment from 'moment';
import { BUTTON_GREY } from '../../colors/base';
import { selectSample } from '../../actions/';

class ListItem extends Component {
  render() {
    return (
      <TouchableOpacity onPress={() => { this.props.selectSample(this.props.sample.id); }}>
        <View style={styles.viewStyle}>
          <View style={{ justifyContent: 'space-between' }}>
            <Text style={{ color: '#CCC' }}>
              Sample Number: <Text style={{ color: '#eee', fontWeight: 'bold' }}>
              {this.props.sample.sample_number}
            </Text>
          </Text>
            <Text style={{ color: '#CCC' }}>
              Sample Date: <Text style={{ color: '#eee', fontWeight: 'bold' }}>
                {moment(this.props.sample.sample_date).format('MMMM Do YYYY')}
              </Text>
            </Text>
            <Text style={{ fontSize: 10, color: '#CCC' }}>
              ({moment(this.props.sample.sample_date).fromNow()})
            </Text>
          </View>
          <Icon
            name='ios-arrow-forward'
            size={45}
            color="#eee"
            style={{
              alignSelf: 'flex-end'
            }}
          />
        </View>
      </TouchableOpacity>
    );
  }

}


const styles = StyleSheet.create({
  viewStyle: {
    backgroundColor: BUTTON_GREY,
    marginBottom: 10,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

export default connect(null, { selectSample })(ListItem);
