import React from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';

const Spinner = (props) => {
  return (
    <ActivityIndicator
      style={styles.centered}
      size={props.size || 'large'}
    />
  );
};

const styles = StyleSheet.create({
  centerd: {
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default Spinner;
