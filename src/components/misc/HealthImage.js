import React from 'react';
// import { Thumbnail } from 'native-base';
import { Image } from 'react-native';

const HealthImage = (props) => {
  if (props.health === 1) {
    return (
      <Image
        square style={{ width: props.size || 60, height: props.size || 60 }}
        source={require('../../assets/images/trans_green.png')}
      />
    );
  } else if (props.health === 2) {
    return (
      <Image
        square style={{ width: props.size || 60, height: props.size || 60 }}
        source={require('../../assets/images/trans_orange.png')}
      />
    );
  } else if (props.health === 3) {
    return (
      <Image
        square style={{ width: props.size || 60, height: props.size || 60 }}
        source={require('../../assets/images/trans_red.png')}
      />
    );
  } else if (props.health === 4) {
    return (
      <Image
        square style={{ width: props.size || 60, height: props.size || 60 }}
        source={require('../../assets/images/trans_red.png')}
      />
    );
  }

  return (<Image
    square style={{ width: props.size || 60, height: props.size || 60 }}
    source={require('../../assets/images/trans_red.png')}
  />);
};

export default HealthImage;
