import React, { Component } from 'react';
import { ScrollView, View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { createSample, sampleUpdate } from '../../actions';
import { BACKGROUND_COLOR, YELLOW } from '../../colors/base';
import SampleForm from '../common/SampleForm';

const WIDTH = Dimensions.get('window').width;

class AddSample extends Component {

  onButtonPress() {
    const {
    source_type,
    sample_date,
    sample_number,
    n2,
    o2,
    ch4,
    c2h2,
    c2h4,
    co2,
    co,
    c2h6,
    h2,
    moisture,
    fol,
  fal,
  ift,
  mef,
  d1816,
  acid,
  acf,
  d877,
  hmf,
  d18162mm,
  furan,
  color

 } = this.props;
    return this.props.createSample({ source_type: source_type || '1',
    sample_date,
    sample_number,
    n2,
    o2,
    ch4,
    c2h2,
    c2h4,
    co2,
    co,
    c2h6,
    h2,
    moisture,
    fol,
  fal,
  ift,
  mef,
  d1816,
  acid,
  acf,
  d877,
  hmf,
  d18162mm,
  furan,
  color
    });
  }
  render() {
    return (
      <ScrollView style={styles.viewStyle}>
        <Text style={styles.headerText}>Input your values</Text>
        <View style={{ height: 0.8, backgroundColor: '#999', marginTop: 10 }} />
        <SampleForm />
        <View style={{ marginTop: 20, marginBottom: 100 }}>
          <TouchableOpacity style={[styles.button, { backgroundColor: YELLOW }]} onPress={this.onButtonPress.bind(this)}>
            <Text style={styles.buttonText}>Add Sample</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  viewStyle: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    paddingTop: 65,
    paddingLeft: 10,
    paddingRight: 10
  },
  headerText: {
    color: '#fff'
  },
  button: {
    borderRadius: 3,
    width: WIDTH - 20,
    borderWidth: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff',
    fontSize: 15
  }
});
function mapStateToProps(state) {
  const { source_type,
  sample_date,
  sample_number,
  n2,
  o2,
  ch4,
  c2h2,
  c2h4,
  co2,
  co,
  c2h6,
  h2,
  moisture
   } = state.samples;
  return {
    source_type,
    sample_date,
    sample_number,
    n2,
    o2,
    ch4,
    c2h2,
    c2h4,
    co2,
    co,
    c2h6,
    h2,
    moisture
  };
}

export default connect(mapStateToProps, { createSample, sampleUpdate })(AddSample);
