import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { ScrollView, StyleSheet, Dimensions, TouchableOpacity, Text, View } from 'react-native';
import { sampleUpdate, saveSample } from '../../actions';
import { BACKGROUND_COLOR, YELLOW } from '../../colors/base';
import SampleForm from '../common/SampleForm';

const WIDTH = Dimensions.get('window').width;

class EditSample extends Component {
  componentWillMount() {
    _.each(this.props.navigation.state.params.sample, (value, prop) => {
      this.props.sampleUpdate({ prop, value });
    });
  }
  onButtonPress() {
    const { source_type,
    sample_date,
    sample_number,
    n2,
    o2,
    ch4,
    c2h2,
    c2h4,
    co2,
    co,
    c2h6,
    h2,
    moisture } = this.props;
    // console.log(sample_date, sample_number);
    this.props.saveSample({ source_type,
    sample_date,
    sample_number,
    n2,
    o2,
    ch4,
    c2h2,
    c2h4,
    co2,
    co,
    c2h6,
    h2,
    moisture });
  }
  render() {
    return (
      <ScrollView style={styles.viewStyle}>
        <SampleForm />
        <View style={{ marginTop: 20, marginBottom: 100 }}>
          <TouchableOpacity style={[styles.button, { backgroundColor: YELLOW }]} onPress={this.onButtonPress.bind(this)}>
            <Text style={styles.buttonText}>Save Changes</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    paddingTop: 65,
    paddingLeft: 10,
    paddingRight: 10
  },
  headerText: {
    color: '#fff'
  },
  button: {
    borderRadius: 3,
    width: WIDTH - 20,
    borderWidth: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff',
    fontSize: 15
  }
});

function mapStateToProps(state) {
  const { source_type,
  sample_date,
  sample_number,
  n2,
  o2,
  ch4,
  c2h2,
  c2h4,
  co2,
  co,
  c2h6,
  h2,
  moisture
   } = state.samples;
  return {
    source_type,
    sample_date,
    sample_number,
    n2,
    o2,
    ch4,
    c2h2,
    c2h4,
    co2,
    co,
    c2h6,
    h2,
    moisture
  };
}

export default connect(mapStateToProps, { sampleUpdate, saveSample })(EditSample);
