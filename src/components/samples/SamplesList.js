import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import _ from 'lodash';
import { connect } from 'react-redux';
// import { NavigationActions } from 'react-navigation';
import { BACKGROUND_COLOR, BUTTON_GREY } from '../../colors/base';
import { samplesFetch } from '../../actions/';
import SampleListItem from '../common/SampleListItem';

class SamplesList extends Component {

  componentWillMount() {
    // Get the samples...
    this.props.samplesFetch();
  }

  renderSamples() {
    if (this.props.samples.length === 0) {
      return (
        <View style={styles.noSampleContainer}>
          <Icon name='bell' size={45} color="#fff" style={styles.bellStyle} />
          <Text
            style={{ color: '#fff', fontSize: 17, fontWeight: 'bold', paddingTop: 5 }}
          > Sorry. No Samples found</Text>
          <Text style={{ color: '#fff', textAlign: 'center', paddingTop: 5 }}>
            Please Press on above
              <Text style={{ color: 'red' }}> Add Sample </Text>
             to add Sample to your asset
          </Text>
        </View>
      );
    }
    return this.props.samples.map(sample =>
      <SampleListItem sample={sample} key={sample.id} />
    );
  }
  render() {
    return (
      <ScrollView style={styles.viewStyle}>
        {this.renderSamples()}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    padding: 10
  },
  noSampleContainer: {
    backgroundColor: BUTTON_GREY,
    padding: 15,
    alignSelf: 'center',
    marginTop: 20,
    alignItems: 'center'
  }
});

const mapStateToProps = state => {
  return {
    samples: _.values(state.samples.sample)
  };
};

export default connect(mapStateToProps, { samplesFetch })(SamplesList);
