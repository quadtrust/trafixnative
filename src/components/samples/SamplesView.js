import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import { fetchSample } from '../../actions/SampleActions';
import { BACKGROUND_COLOR, BUTTON_GREY, YELLOW } from '../../colors/base';

const WIDTH = Dimensions.get('window').width;

class SamplesView extends Component {

  componentWillMount() {
    this.props.fetchSample();
  }
  render() {
    console.log('Sample PROP:', this.props.sample.asset_name);
    const {
      h2, c2h2, c2h4,
      c2h6, ch4, co, co2,
      moisture, n2, o2,
      sample_number, sample_date
    } = this.props.sample;

    return (
      <ScrollView style={styles.sampleViewContainerStyle}>

        <View style={{ marginBottom: 50, marginTop: 20 }}>
          <View
            style={{
            backgroundColor: BUTTON_GREY,
            padding: 10,
            marginBottom: 10,
            alignItems: 'center',
            justifyContent: 'center'
          }}
          >
            <Text
              style={{
                fontSize: 15,
                color: '#fff',
              }}
            >Sample Number: {sample_number}
            </Text>
            <Text
              style={{
                fontSize: 15,
                color: '#fff',
              }}
            >Sample Date: {moment(sample_date).format('MMMM Do YYYY')}
            </Text>
          </View>
          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataHeaderText}>Name of Gas</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataHeaderText}>Value</Text>
            </View>
          </View>
          <View style={styles.dividerStyle} />

          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>H2</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>{h2}</Text>
            </View>
          </View>
          <View style={styles.dividerStyle} />

          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>C2H2</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>{c2h2}</Text>
            </View>
          </View>
          <View style={styles.dividerStyle} />

          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>C2H4</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>{c2h4}</Text>
            </View>
          </View>
          <View style={styles.dividerStyle} />

          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>C2H6</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>{c2h6}</Text>
            </View>
          </View>
          <View style={styles.dividerStyle} />

          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>O2</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>{o2}</Text>
            </View>
          </View>
          <View style={styles.dividerStyle} />

          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>CO2</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>{co2}</Text>
            </View>
          </View>
          <View style={styles.dividerStyle} />

          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>CO</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>{co}</Text>
            </View>
          </View>
          <View style={styles.dividerStyle} />

          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>Moisture</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>{moisture}</Text>
            </View>
          </View>
          <View style={styles.dividerStyle} />

          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>N2</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>{n2}</Text>
            </View>
          </View>
          <View style={styles.dividerStyle} />

          <View style={styles.sampleDataStyle}>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>CH4</Text>
            </View>
            <View style={styles.sampleHeadingStyle}>
              <Text style={styles.sampleDataTextStyle}>{ch4}</Text>
            </View>
          </View>

          <TouchableOpacity
            style={[styles.button, { marginTop: 20, borderColor: YELLOW }]}
            onPress={() => {
              // Actions.diagnostics();
              this.props.navigation.navigate('diagnostics');
            }}
          >
            <Text style={styles.buttonText}>
              View Diagnostic Report
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[styles.button, { marginTop: 20, borderColor: YELLOW }]}
            onPress={() => {
              // Actions.editsample({ sample: this.props.sample });
              this.props.navigation.navigate('editSample', { sample: this.props.sample });
            }}
          >
            <Text style={styles.buttonText}>
              Edit Sample
            </Text>
          </TouchableOpacity>

        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  sampleViewContainerStyle: {
    backgroundColor: BACKGROUND_COLOR,
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
    paddingBottom: 50
  },
  dividerStyle: {
    height: 0.7,
    backgroundColor: '#999',
    marginTop: 8,
    marginBottom: 8
  },
  sampleDataStyle: {
    flexDirection: 'row'
  },
  sampleDataTextStyle: {
    color: '#fff',
    fontSize: 15
  },
  sampleDataHeaderText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 18
  },
  sampleHeadingStyle: {
    flex: 1,
    alignItems: 'center'
  },
  button: {
    borderRadius: 3,
    width: WIDTH - 20,
    borderWidth: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff',
    fontSize: 15
  }
});

const mapStateToProps = state => {
  return {
    sample: state.samples.singleSample.sample
  };
};

export default connect(mapStateToProps, { fetchSample })(SamplesView);
