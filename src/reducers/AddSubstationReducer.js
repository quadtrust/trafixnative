import {
  SUBSTATION_CHANGED,
  SUBSTATION_POST_SUCCESS,
  LOAD_SUB_SPINNER,
  SUBSTATION_POST_FAILED
} from '../actions/types';

const INITIAL_STATE = {
  substation: '',
  loading: false,
  error: ''
};
export default (state = INITIAL_STATE, action) => {
  console.log(action.payload);
  switch (action.type) {
    case SUBSTATION_POST_FAILED:
    return { ...state, error: 'Sorry. Substation post failed' };
    case LOAD_SUB_SPINNER:
    return { ...state, loading: true };
    case SUBSTATION_POST_SUCCESS:
    return { ...state, ...INITIAL_STATE };
    case SUBSTATION_CHANGED:
    return { ...state, substation: action.payload };
    default:
    return state;
  }
};
