import {
  ASSETS_FETCHED_SUCCESS,
  LOAD_ASSET_SPINNER,
  ASSET_ID_SELECT,
  ASSET_FETCHED_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
  singleAsset: {
    asset: []
  }
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOAD_ASSET_SPINNER:
      return { ...state, asset_spinner: true };
    case ASSETS_FETCHED_SUCCESS:
      return { ...state, ...action.payload, asset_spinner: false };
    case ASSET_FETCHED_SUCCESS:
      return { ...state, ...action.payload, asset_spinner: false };
      // return action.payload;
    case ASSET_ID_SELECT:
      return { ...state, selectedAsset: action.payload };
    default:
      return state;
  }
};
