import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED,
  LOAD_SPINNER
} from '../actions/types';

const INITIAL_STATE = {
  email: '',
  password: '',
  error: '',
  spinner: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case EMAIL_CHANGED:
      return { ...state, email: action.payload };
    case PASSWORD_CHANGED:
      return { ...state, password: action.payload };
    case LOGIN_USER_SUCCESS:
      return { ...state,
         ...action.payload,
         ...INITIAL_STATE
      };
    case LOGIN_USER_FAILED:
      return {
        ...state,
        password: '',
        error: 'Authentication Error. Please try again.',
        spinner: false
      };
    case LOAD_SPINNER:
      return { ...state, spinner: true };
    default:
      return state;
  }
};
