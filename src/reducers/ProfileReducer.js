import {
  PROFILE_FETCHED_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
  profile: {},
  user: {}
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PROFILE_FETCHED_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
