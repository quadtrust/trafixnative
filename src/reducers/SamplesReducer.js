import {
  SAMPLES_FETCHED_SUCCESS,
  SAMPLE_ID_SELECT,
  SAMPLE_FETCHED_SUCCESS,
  DIAGNOSTICS_FETCHED_SUCCESS,
  SAMPLE_UPDATE,
  SAMPLE_CREATE,
  SAVE_EDIT_SAMPLE
} from '../actions/types';

const INITIAL_STATE = {
  singleSample: {
    sample: []
  },
  diagnostics: {
    diagnostics: []
  }
};
const SAMPLE_INITIAL_STATE = {
  source_type: '1',
  sample_date: '',
  sample_number: '',
  n2: '',
  o2: '',
  ch4: '',
  c2h2: '',
  c2h4: '',
  co2: '',
  co: '',
  c2h6: '',
  h2: '',
  moisture: ''
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SAMPLES_FETCHED_SUCCESS:
      return { ...state, ...action.payload };
    case SAMPLE_ID_SELECT:
      return { ...state, selectedSample: action.payload };
    case SAMPLE_FETCHED_SUCCESS:
      return { ...state, ...action.payload };
    case DIAGNOSTICS_FETCHED_SUCCESS:
      return { ...state, diagnostics: action.payload };
    case SAMPLE_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };
    case SAMPLE_CREATE:
      return { ...state, ...SAMPLE_INITIAL_STATE };
    case SAVE_EDIT_SAMPLE:
      return { ...state, ...SAMPLE_INITIAL_STATE };
    default:
      return state;
  }
};
