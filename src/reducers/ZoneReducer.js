import {
  ZONES_FETCHED_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ZONES_FETCHED_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
