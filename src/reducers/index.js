import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import AssetsReducer from './AssetsReducers';
import SamplesReducer from './SamplesReducer';
import ProfileReducer from './ProfileReducer';
import ZoneReducer from './ZoneReducer';
import AddSubstation from './AddSubstationReducer';

export default combineReducers({
  auth: AuthReducer,
  assets: AssetsReducer,
  samples: SamplesReducer,
  profile: ProfileReducer,
  zones: ZoneReducer,
  substations: AddSubstation
});
