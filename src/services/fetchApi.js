import fetchival from 'fetchival';
import _ from 'lodash';
import store from '../store';
import { API_ENDPOINT } from '../actions/api';

export const fetchApi = (endPoint, payload = {}, method = 'get', headers = {}) => {
	const accessToken = store.getState().authentication_token;
	return fetchival(`${API_ENDPOINT}${endPoint}`, {
		headers: _.pickBy({
			...(accessToken ? {
				'X-User-Token': `${accessToken}`,
			} : {
				'Client-ID': 'Not Logged In',
			}),
			...headers,
		}, item => !_.isEmpty(item)),
	})[method.toLowerCase()](payload);
};
